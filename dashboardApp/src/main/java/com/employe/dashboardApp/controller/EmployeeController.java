package com.employe.dashboardApp.controller;

import com.employe.dashboardApp.entity.Employee;
import com.employe.dashboardApp.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class EmployeeController {
    @Autowired
    private EmployeeService service;

    @PostMapping("/addEmployee")
    public Employee addEmployee(@RequestBody Employee employee){
        return service.saveEmployee(employee);
    }

    @PostMapping("/addEmployees")
    public List<Employee> addEmployees(@RequestBody List<Employee> employees){
        return service.saveEmployees(employees);
    }

    @GetMapping("/employees")
    public List<Employee> findAllEmployees(){
        return service.getEmployees();
    }

//    @PutMapping("/updateEmployee")
//    public List<Employee> update(){
//        return service.getEmployees();
//    }

    @PutMapping("/update/{id}")
    public void updateEmployee(@RequestBody Employee employee, @PathVariable int id){
        employee.setId(id);
        service.saveEmployee(employee);
    }

    @GetMapping("/employee/{id}")
    public Employee findEmployeeById(@PathVariable int id){
        return service.getEmployeeById(id);
    }

    @GetMapping("/employeeByName/{nama}")
    public Employee findEmployeeByName(@PathVariable String nama){
        return service.getEmployeeByName(nama);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteEmployee(@PathVariable int id){
        return  service.deleteEmployee(id);
    }

}
