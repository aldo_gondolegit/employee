function getData(url) {
    let xmlhttp = new XMLHttpRequest()
    xmlhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let resp2 = JSON.parse(this.responseText)
            let myName = ""

            resp2.forEach(namaNya => {
                console.log(namaNya); //ngecek data
                myName +=
                `<div id="${namaNya.id}" onclick="showById(id)" class="list-group-item list-group-item-action bg-light">${namaNya.name}</div>`
            })
        
            document.getElementById("sidebar").innerHTML = myName ;

        }

    }
    xmlhttp.open("GET",url,true)
    xmlhttp.send()
}
getData("http://localhost:8888/employees")

function showDetail(element) {
    let myResponse = ""

        myResponse = 
                `
                <h6>${element.nik}</h6>
                <h6>${element.name}</h6>
                <h6>${element.ttl}</h6>
                <h6>${element.jk}</h6>
                <h6>${element.agama}</h6>
                <h6>${element.kewarganegaraan}</h6>
                <h6>${element.alamat}</h6>
                <button onClick=deleteID(${element.id}) class="btn btn-danger mt-5" type="submit" id="hapus">Hapus</button>
                <button class="btn btn-primary mt-5" data-toggle="modal" data-target="#staticBackdrop" type="submit" onclick="getUpdate(${element.id})">
                Edit
                </button>
                `
    document.getElementById("isidata").innerHTML = myResponse;

}

function showById(id) {
     fetch(`http://localhost:8888/employee/`+id,{
        method: 'GET',
    })
    .then(res => {
        return res.json();
    })
    .then (data => showDetail(data))
    .catch(console.error);
}

// const queryString = window.location.search;
// const urlParams = new URLSearchParams(queryString);
// const idParams = urlParams.get('id')
// console.log(idParams);

const modalUpdate = (data) => {
    let myResp = ""
    
    myResp = 
    `
            <!-- Modal -->
            <div class="row">
            <input type="number" class="form-control col-md-5" placeholder="NIK" id="nik" value"${data.nik}">
            
            <input type="text" class="form-control col-md-5 offset-1" placeholder="Nama" id="nama" value"${data.nama}">
            
            </div>
                    <br>
                    <div class="row">
                    <input type="text" class="form-control col-md-5" placeholder="Tempat, Tanggal Lahir" id="ttl" value"${data.ttl}">
                    
                    <input type="text" class="form-control col-md-5 offset-1" placeholder="Jenis Kelaman" id="jk" value"${data.jk}">
                    
                    </div>
                    <br>
                    <div class="row">
                    <input type="text" class="form-control col-md-5" placeholder="Agama" id="agama" value"${data.agama}">
                    
                    <input type="text" class="form-control col-md-5 offset-1" placeholder="Kewarganegaraan" id="kewarganegaraan" value"${data.kewarganegaraan}">
                    </div>
                    <br>
                    <div class="row">
                    <input type="text" class="form-control col-md-5" placeholder="Alamat" id="alamat" value"${data.alamat}">
                    
                    </div>
                    <div class="modal-footer">
                    <button class="btn btn-success offset-1" type="submit" id="update" onclick="update(${data.id})">update</button>
                    </div>
                    `
                    document.getElementById("dataUpdate").innerHTML = myResp;
                    
                }
            
                function getUpdate(id) {
                    fetch(`http://localhost:8888/employee/`+ id,{
                        method: 'GET',
                    })
                    .then(res => {
                        return res.json();
                    })
                    .then (data => modalUpdate(data))
                    .catch(console.error);
                }
                // getUpdate();

function deleteID(id) {
    fetch(`http://localhost:8888/delete/`+id,{
        method: 'DELETE',
    })
    .then(res => location.reload())
    alert("DATA DIHAPUS");
}

function update(id) {

    var nik = document.getElementById("nik").value
    var nama = document.getElementById("nama").value
    var ttl = document.getElementById("ttl").value
    var jk = document.getElementById("jk").value
    var agama = document.getElementById("agama").value
    var kewarganegaraan = document.getElementById("kewarganegaraan").value
    var alamat = document.getElementById("alamat").value

    var json_update = JSON.stringify({
        nik: nik,
        name: nama,
        ttl: ttl,
        jk: jk,
        agama: agama,
        kewarganegaraan: kewarganegaraan,
        alamat: alamat
    })

    fetch(`http://localhost:8888/update/`+ id,{
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
        },
        body: json_update,
    })
    window.location.href = "index.html"
    alert("BERHASIL UPDATE")
    
}

    