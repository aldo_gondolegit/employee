package com.employe.dashboardApp.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "daftarkaryawan")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private int nik;
    private String name;
    private String ttl;
    private String jk;
    private String agama;
    private String kewarganegaraan;
    private String alamat;

    public void setId(int id) {
        this.id = id;
    }
}
