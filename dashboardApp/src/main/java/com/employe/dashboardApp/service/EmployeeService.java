package com.employe.dashboardApp.service;


import com.employe.dashboardApp.entity.Employee;
import com.employe.dashboardApp.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository repository;

    public Employee saveEmployee(Employee employee) {

        return repository.save(employee);
    }

    public List<Employee> saveEmployees(List<Employee> employees) {
        return repository.saveAll(employees);
    }

    public List<Employee> getEmployees() {
        return repository.findAll();
    }

//    public List<Employee> getName(){
//
//    }

    public Employee getEmployeeById(int id){

        return repository.findById(id).orElse(null);
    }

    public Employee getEmployeeByName(String nama){

        return repository.findByName(nama);
    }

    public String deleteEmployee(int id){
        repository.deleteById(id);
        return "Employee Removed!";
    }

//    public Employee updateEmployee(Employee employee){
//        Employee existingProduct = repository.findById(employee.getId()).orElse(null);
//        existingProduct.setName(employee.getName());
//        existingProduct.setQty(employee.getQty());
//        existingProduct.setPrice(employee.getPrice());
//        return repository.save(existingProduct);
//    }
}

