package com.employe.dashboardApp.repository;

import com.employe.dashboardApp.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository  extends JpaRepository<Employee, Integer> {
    Employee findByName(String nama);
}
